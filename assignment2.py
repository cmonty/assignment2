from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
	r = json.dumps(books)
	return Response(r)

@app.route('/')
@app.route('/book/')
def showBook():
	return render_template('index.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
	if request.method == 'POST':
		new_name = request.form['name']
		past_id = 0
		for i in range(len(books)):
			cur_id = int(books[i]["id"])
			if cur_id - past_id > 1:
				books.insert(past_id, {'title': new_name, 'id': str(past_id + 1)})
				break
			if i == (len(books) - 1):
				books.append({'title': new_name, 'id': len(books) + 1})
			past_id = cur_id

		#return redirect(url_for('showBook', books = books))
		return redirect(url_for('newBook', books=books))
	else:
		return render_template('newbook.html')


@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
	if request.method == 'POST':
		edited_name = request.form['name']
		for i in books:
			if i["id"] == str(book_id):
				i["title"] = edited_name
				break

		return redirect(url_for('showBook', books = books))
	else:
		return render_template('editbook.html', book_id = book_id)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
	if request.method == 'POST':
		for i in range(len(books)):
			if books[i]["id"] == str(book_id):
				books.pop(i)
				break
		return redirect(url_for('showBook', books = books))
	else:
		return render_template('deletebook.html', book_id = book_id)


if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

